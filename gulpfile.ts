import * as gulp from 'gulp';
import * as util from 'gulp-util';
import * as runSequence from 'run-sequence';

import Config from './tools/config';
import { loadTasks, loadCompositeTasks } from './tools/utils';


loadTasks(Config.SEED_TASKS_DIR);
loadTasks(Config.PROJECT_TASKS_DIR);

loadCompositeTasks(Config.SEED_COMPOSITE_TASKS, Config.PROJECT_COMPOSITE_TASKS);


// --------------
// Clean dev/coverage that will only run once
// this prevents karma watchers from being broken when directories are deleted
let firstRun = true;
gulp.task('clean.once', (done: any) => {
  if (firstRun) {
    firstRun = false;
    runSequence('check.tools', 'clean.dev', 'clean.coverage', done);
  } else {
    util.log('Skipping clean on change again');
    done();
  }
});
gulp.task('test', (done:any)=>{
 runSequence('build.test',
              'karma.run',done)
});
  
gulp.task('build.test',(done:any)=>{
runSequence('clean.once','build.assets.dev',
              'build.html_css','build.js.dev',
              'build.js.test','build.index.dev',done)
});

//Build Prod task
gulp.task('build.prod', (done:any)=>{
  runSequence('clean.prod','tslint',
  'build.assets.prod','build.html_css',
  'copy.prod','build.js.prod','build.bundles.app',
  'minify.bundles','buid.index.prod',done)
});