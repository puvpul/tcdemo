# Customer Registration Source


This repository holds the Angular2 and TypeScript source code of the [Customer Registration]
the foundation for customer registration application.

**This is just a skeleton source code and not the perfect arrangement of the application.
## Prerequisites

Node.js and npm are essential to Angular development. 
    
<a href="https://docs.npmjs.com/getting-started/installing-node" target="_blank" title="Installing Node.js and updating npm">
Get it now</a> if it's not already installed on your machine.
 
**Verify that you are running at least node `v4.x.x` and npm `3.x.x`**
by running `node -v` and `npm -v` in a terminal/console window.
Older versions produce errors.

## Create a new project based on the QuickStart

Clone this repo into new project folder (e.g., `consumer-registration`).
```shell
git clone https://bitbucket.org/mydnalife/consumer-registration.git  consumer-registration
cd consumer-registration
```