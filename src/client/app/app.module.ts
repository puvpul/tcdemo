import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF } from '@angular/common';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { MyDNACoreModule } from './core/mydna-core.module';
import { CustomerRegistrationWizardModule } from './customer-registration-wizard/customer-registration-wizard.module';
import { WildcardRoutingModule } from './core/wildcard-routing.module';

@NgModule({
  imports: [BrowserModule, HttpModule, AppRoutingModule, CustomerRegistrationWizardModule, MyDNACoreModule.forRoot(), WildcardRoutingModule],
  declarations: [AppComponent],
  providers: [{
    provide: APP_BASE_HREF,
    useValue: '<%= APP_BASE %>'
  }],
  bootstrap: [AppComponent]

})
export class AppModule { }
