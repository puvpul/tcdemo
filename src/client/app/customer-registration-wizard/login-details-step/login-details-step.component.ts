import { Component, OnInit } from '@angular/core';
import { WizardStep } from '../wizard-step';
import { RegistrationService } from '../services/registration.service';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'mydna-customerreg-login-details-step',
  templateUrl: 'login-details-step.component.html',
  styleUrls: ['login-details-step.component.css']
})
export class LoginDetailsStepComponent extends WizardStep implements OnInit {

    constructor(public registrationService : RegistrationService) {
      super(registrationService);
    }

  public ngOnInit() { 
    this.stepName = 'login-details-step';
    this.title = 'User Details';
    this.instructionalText = 'Lorem Ipsum......';
  }
}
