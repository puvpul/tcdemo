import { Component, OnInit } from '@angular/core';
import { WizardStep } from '../wizard-step';
import { RegistrationService } from '../services/registration.service';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'mydna-customerreg-medications-step',
  templateUrl: 'medications-step.component.html',
  styleUrls: ['medications-step.component.css']
})
export class MedicationsStepComponent extends WizardStep implements OnInit {

    constructor(public registrationService : RegistrationService) {
      super(registrationService);
    }

  public ngOnInit() {
    this.stepName = 'medications-step';
    this.title = 'Medications';
    this.instructionalText = 'Lorem Ipsum......';
  }

}
