import { Component, Input, ElementRef, Renderer , forwardRef } from '@angular/core'
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

export interface DateDropDownProperties {
    dateFormat: any;

    dayDivClass: any;
    dayClass: any;
    monthDivClass: any;
    monthClass: any;
    yearDivClass: any;
    yearClass: any;
}

const noop = () => {
};

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => DateDropDownComponent),
    multi: true
};

@Component({
    moduleId: module.id,
    selector: 'date-dropdown',
    templateUrl: 'date-dropdown.component.html',
    styleUrls: ['date-dropdown.component.css'],
    providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]

})

export class DateDropDownComponent implements ControlValueAccessor {
    dayRange: any;
    monthsArray: any

    dayList: any;
    MonthList: any;
    YearList: any;

    dateFields: any;
    date: any;

    dateIsRequired:boolean;

   
    @Input() dateFormat: any = "dd/MM/yyyy";

    @Input() dayDivClass: any = "day-container";
    @Input() dayClass: any = "day-selector";
    @Input() monthDivClass: any = "month-container";
    @Input() monthClass: any = "month-selector";
    @Input() yearDivClass: any = "year-container";
    @Input() yearClass: any = "year-selector";

     
    constructor(private el: ElementRef, private renderer: Renderer) {
        this.dayRange = [1, 31]
        this.monthsArray = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ];

        this.dateFields = {};
        this.dayList = [];
        this.MonthList = [];
        this.YearList = [];

        this.GetdDays();
        this.GetMonths();
        this.GetYears();

        this.date = ""
       // this.setCssClasses();      
         }


    //The internal data model
    private innerValue: any = '';

    //Placeholders for the callbacks which are later provided
    //by the Control Value Accessor
    
    private onTouchedCallback: () => void = noop;
     private onChangeCallback : (_: any) => void = noop;
   
        
    
    

    //get accessor
    get value(): any {
        return this.innerValue;
    };

    //set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;               
            this.onChangeCallback(v);
        }
    }

    //Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    //From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
            // this call is to rebind the value to model.
            this.onModelChange();
        }
    }

    //From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    //From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }


    setCssClasses() {
        var cssArray: any = [this.dayDivClass, this.monthDivClass, this.yearDivClass, this.dayClass, this.monthClass, this.yearClass];
        for (let i = 0; i < cssArray.length; i++) {
            this.renderer.setElementClass(this.el.nativeElement, cssArray[i], true);
        }

    }

    GetdDays(): void {
        var days: any = [];
        while (this.dayRange[0] <= this.dayRange[1]) {
            days.push(this.dayRange[0]++);
        }
        this.dayList = days;

    };


    GetMonths(): void {
        var lst: any = [],
            mLen = this.monthsArray.length;

        for (var i = 0; i < mLen; i++) {
            lst.push({
                value: i,
                name: this.monthsArray[i]
            });
        }
        this.MonthList = lst;
    }

    GetYears(): void {
        var lst: any = [];
        var currentYear = new Date().getFullYear();
        var numYears: number = 150;
        var oldestYear = currentYear - numYears;
        for (var i = currentYear; i >= oldestYear; i--) {
            lst.push(i);
        }

        this.YearList = lst;
    }

    changeDate(date: any) {
        if (date.day > 28) {
            date.day--;
            return date;
        } else if (date.month > 11) {
            date.day = 31;
            date.month--;
            return date;
        }
    }

    formatDate(date: any, format: any) {

        var day = date.getDate();
        var month = date.getMonth()+1;
        var year = date.getFullYear();

        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }

        if (format = 'dd/MM/yyyy') {
            return day + '/' + month + '/' + year;
        }
        else {
            //'MM/dd/yyyy'       
            return month + '/' + day + '/' + year;
        }
    }

    checkDate(date: any): any {
        var d: any;
        if (!date.day || date.month === null || date.month === undefined || !date.year) return false;

        // d = new Date(Date.UTC(date.year, date.month, date.day));
        d = new Date(date.year, date.month, date.day);

        if (d && (d.getMonth() === date.month && d.getDate() === Number(date.day))) {
            return d;
        }

        return this.checkDate(this.changeDate(date));
    }

    OnChangeDate() {       
        var date = this.checkDate(this.dateFields);
        if (date) {
            var newdate = this.formatDate(date, this.dateFormat);
            this.date = newdate;
            this.value = this.date;
        }

    }

    onModelChange() {
      
        if (this.value!==undefined  && this.value!="" && this.value!=null){
            var newDate = this.value;
            var dateParts = newDate.split("/");
            this.dateFields.day = parseInt(dateParts[0]);
            this.dateFields.month = parseInt(dateParts[1])-1;
            this.dateFields.year = parseInt(dateParts[2]); 
        }else{
             this.dateFields.day = new Date(this.value).getDate();
             this.dateFields.month = new Date(this.value).getMonth();
             this.dateFields.year = new Date(this.value).getFullYear();
        }
       
    }



   


    


}