"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var noop = function () {
};
exports.CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = {
    provide: forms_1.NG_VALUE_ACCESSOR,
    useExisting: core_1.forwardRef(function () { return DateDropDownComponent; }),
    multi: true
};
var DateDropDownComponent = (function () {
    function DateDropDownComponent(el, renderer) {
        this.el = el;
        this.renderer = renderer;
        this.dateFormat = "dd/MM/yyyy";
        this.dayDivClass = "day-container";
        this.dayClass = "day-selector";
        this.monthDivClass = "month-container";
        this.monthClass = "month-selector";
        this.yearDivClass = "year-container";
        this.yearClass = "year-selector";
        //The internal data model
        this.innerValue = '';
        //Placeholders for the callbacks which are later provided
        //by the Control Value Accessor
        this.onTouchedCallback = noop;
        this.onChangeCallback = noop;
        this.dayRange = [1, 31];
        this.monthsArray = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ];
        this.dateFields = {};
        this.dayList = [];
        this.MonthList = [];
        this.YearList = [];
        this.GetdDays();
        this.GetMonths();
        this.GetYears();
        this.date = "";
        this.setCssClasses();
    }
    DateDropDownComponent.prototype.setCssClasses = function () {
        var cssArray = [this.dayDivClass, this.monthDivClass, this.yearDivClass, this.dayClass, this.monthClass, this.yearClass];
        for (var i = 0; i < cssArray.length; i++) {
            this.renderer.setElementClass(this.el.nativeElement, cssArray[i], true);
        }
    };
    DateDropDownComponent.prototype.GetdDays = function () {
        var days = [];
        while (this.dayRange[0] <= this.dayRange[1]) {
            days.push(this.dayRange[0]++);
        }
        this.dayList = days;
    };
    ;
    DateDropDownComponent.prototype.GetMonths = function () {
        var lst = [], mLen = this.monthsArray.length;
        for (var i = 0; i < mLen; i++) {
            lst.push({
                value: i,
                name: this.monthsArray[i]
            });
        }
        this.MonthList = lst;
    };
    DateDropDownComponent.prototype.GetYears = function () {
        var lst = [];
        var currentYear = new Date().getFullYear();
        var numYears = 150;
        var oldestYear = currentYear - numYears;
        for (var i = currentYear; i >= oldestYear; i--) {
            lst.push(i);
        }
        this.YearList = lst;
    };
    DateDropDownComponent.prototype.changeDate = function (date) {
        if (date.day > 28) {
            date.day--;
            return date;
        }
        else if (date.month > 11) {
            date.day = 31;
            date.month--;
            return date;
        }
    };
    DateDropDownComponent.prototype.formatDate = function (date, format) {
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }
        if (format = 'dd/MM/yyyy') {
            return day + '/' + month + '/' + year;
        }
        else {
            //'MM/dd/yyyy'       
            return month + '/' + day + '/' + year;
        }
    };
    DateDropDownComponent.prototype.checkDate = function (date) {
        var d;
        if (!date.day || date.month === null || date.month === undefined || !date.year)
            return false;
        // d = new Date(Date.UTC(date.year, date.month, date.day));
        d = new Date(date.year, date.month, date.day);
        if (d && (d.getMonth() === date.month && d.getDate() === Number(date.day))) {
            return d;
        }
        return this.checkDate(this.changeDate(date));
    };
    DateDropDownComponent.prototype.OnChangeDate = function () {
        //this.GetdDays();
        var date = this.checkDate(this.dateFields);
        if (date) {
            var newdate = this.formatDate(date, this.dateFormat);
            this.date = newdate;
            this.value = this.date;
            console.log(this.date);
        }
    };
    DateDropDownComponent.prototype.onModelChange = function () {
        if (this !== undefined && this.date != "") {
            var newDate = this.date;
            var dateParts = newDate.split("/");
            this.dateFields.day = parseInt(dateParts[0]);
            this.dateFields.month = parseInt(dateParts[1]) - 1;
            this.dateFields.year = parseInt(dateParts[2]);
        }
        else {
            this.dateFields.day = new Date(this.date).getDate();
            this.dateFields.month = new Date(this.date).getMonth();
            this.dateFields.year = new Date(this.date).getFullYear();
        }
        // if (this.date) {
        //     // if (this.date !== undefined && this.date != "") {
        //     //     this.dateFields.day = new Date(this.date).getDate();
        //     //     this.dateFields.month = new Date(this.date).getMonth();
        //     //     this.dateFields.year = new Date(this.date).getFullYear();
        //     // }
        //     if (this.date !== undefined && this.date != "") {
        //         var dateParts = this.date.split("/");
        //         this.dateFields.day = parseInt(dateParts[0]);
        //         this.dateFields.month = parseInt(dateParts[1]) - 1;
        //         this.dateFields.year = parseInt(dateParts[2]);
        //     }
        // }
    };
    Object.defineProperty(DateDropDownComponent.prototype, "value", {
        //get accessor
        get: function () {
            return this.innerValue;
        },
        //set accessor including call the onchange callback
        set: function (v) {
            if (v !== this.innerValue) {
                this.innerValue = v;
                this.onChangeCallback(v);
            }
        },
        enumerable: true,
        configurable: true
    });
    ;
    //Set touched on blur
    DateDropDownComponent.prototype.onBlur = function () {
        this.onTouchedCallback();
    };
    //From ControlValueAccessor interface
    DateDropDownComponent.prototype.writeValue = function (value) {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    };
    //From ControlValueAccessor interface
    DateDropDownComponent.prototype.registerOnChange = function (fn) {
        this.onChangeCallback = fn;
    };
    //From ControlValueAccessor interface
    DateDropDownComponent.prototype.registerOnTouched = function (fn) {
        this.onTouchedCallback = fn;
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], DateDropDownComponent.prototype, "dateFormat", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], DateDropDownComponent.prototype, "dayDivClass", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], DateDropDownComponent.prototype, "dayClass", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], DateDropDownComponent.prototype, "monthDivClass", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], DateDropDownComponent.prototype, "monthClass", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], DateDropDownComponent.prototype, "yearDivClass", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], DateDropDownComponent.prototype, "yearClass", void 0);
    DateDropDownComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'date-dropdown',
            templateUrl: 'date-dropdown.component.html',
            styleUrls: ['date-dropdown.component.css'],
            providers: [exports.CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, core_1.Renderer])
    ], DateDropDownComponent);
    return DateDropDownComponent;
}());
exports.DateDropDownComponent = DateDropDownComponent;
//# sourceMappingURL=date-dropdown.component.js.map