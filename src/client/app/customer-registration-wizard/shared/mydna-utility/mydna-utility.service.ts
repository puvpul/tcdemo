import { Injectable } from '@angular/core';

@Injectable()
export class MyDnaUtilityService {

    constructor() { }

    public splitPhoneNumber(strphonenumber: any, country: any): any {
        var output = {};
        var PhoneNumber = "";
        var countryCode = "";
        if (strphonenumber !== '' && strphonenumber !== undefined && strphonenumber !== null) {
        PhoneNumber = strphonenumber;
        if (country && country === 'Australia') {
            countryCode = PhoneNumber.substring(0, 3);
            if (countryCode == "+61") {
            PhoneNumber = PhoneNumber.substring(3, PhoneNumber.length)
            }
        } else if (country && country === 'Canada') {
            countryCode = PhoneNumber.substring(0, 2);
            if (countryCode == "+1") {
            PhoneNumber = PhoneNumber.substring(2, PhoneNumber.length)
            }
        }
        }
        output = {
        "CountryCode": countryCode,
        "PhoneNumber": PhoneNumber
        }
        return output;
  }


}