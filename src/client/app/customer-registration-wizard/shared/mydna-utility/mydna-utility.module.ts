import { NgModule } from '@angular/core';

import {MyDnaUtilityService}   from './mydna-utility.service';

@NgModule({
    imports: [],
    exports: [],
    declarations: []  ,
    providers:[MyDnaUtilityService] 
})
export class MyDnaUtilityModule { }
