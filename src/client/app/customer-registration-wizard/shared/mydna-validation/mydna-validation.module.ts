import { NgModule } from '@angular/core';

import { MyDnaValidationService }   from './mydna-validation.service';
import { MyDnaValidationPatterns } from './mydna-validation-patterns'

@NgModule({
    imports: [],
    exports: [],
    declarations: [],
    providers:[MyDnaValidationService]
 
})
export class  MyDnaValidationModule { }
