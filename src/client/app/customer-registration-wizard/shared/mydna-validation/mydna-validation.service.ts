import { Injectable } from '@angular/core';
import { MyDnaValidationPatterns} from './mydna-validation-patterns'

@Injectable()
export class MyDnaValidationService {
    test:any;
    constructor(private myDnaValidationPatterns:MyDnaValidationPatterns) {
        this.test = this.myDnaValidationPatterns.email;
     }
}