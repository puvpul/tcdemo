/**
 * name
 */
export class MyDnaValidationPatterns {

     email:string;
     phone:string;
     mobile:string;
     postcode:string;
     name:string;

    constructor() {
        this.email = "^$|^[-_A-Za-z0-9]+(\.[-_A-Za-z0-9]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,100})$";
        this.postcode = "^[0-9]{4,5}$";  
        this.phone = "^[a-zA-Z0-9 ]{8,12}$";
        this.mobile = "^[a-zA-Z0-9 ]{10,}$";
        this.name = "^([a-zA-Z0-9À-ÿ- ']*)$" 
    }
}