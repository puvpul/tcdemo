import { Component, OnInit } from '@angular/core';
import { WizardStep } from '../wizard-step';
import { RegistrationService } from '../services/registration.service';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'mydna-customerreg-payment-step',
  templateUrl: 'payment-step.component.html',
  styleUrls: ['payment-step.component.css']
})
export class PaymentStepComponent extends WizardStep implements OnInit {

    constructor(public registrationService : RegistrationService) {
      super(registrationService);
    }

  public ngOnInit() { 
    this.stepName = 'payment-step';
    this.title = 'Payment Details';
    this.instructionalText = 'Lorem Ipsum......';
  }
}
