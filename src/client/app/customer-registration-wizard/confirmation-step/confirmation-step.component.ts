import { Component, OnInit } from '@angular/core';
import { WizardStep } from '../wizard-step';
import { RegistrationService } from '../services/registration.service';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'mydna-customerreg-confirmation-step',
  templateUrl: 'confirmation-step.component.html',
  styleUrls: ['confirmation-step.component.css']
})
export class ConfirmationStepComponent extends WizardStep implements OnInit {

    constructor(public registrationService : RegistrationService) {
      super(registrationService);
    }

  public ngOnInit() {
    this.stepName = 'confirmation-step';
    this.title = 'My Kit';
    this.instructionalText = 'Lorem Ipsum......';
  }
}
