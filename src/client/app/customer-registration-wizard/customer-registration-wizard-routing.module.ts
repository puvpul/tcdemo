import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { StartStepComponent } from './start-step/start-step.component';
import { CustomerRegistrationWizardComponent } from './customer-registration-wizard.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'customer-registration', component: CustomerRegistrationWizardComponent },
      { path: 'kit-registration', component: CustomerRegistrationWizardComponent}
      // { path: 'cusotmer-details',component:CustomerRegistrationWizardComponent}
    ])
  ],
  exports: [RouterModule]
})
export class CustomerRegistrationWizardRoutingModule { }
