import { Component, OnInit } from '@angular/core';
import { WizardStep } from '../wizard-step';
import { RegistrationService } from '../services/registration.service';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'mydna-customerreg-sorry-wizard-panel',
  templateUrl: 'sorry-wizard-panel.component.html',
  styleUrls: ['sorry-wizard-panel.component.css']
})
export class SorryWizardPanel extends WizardStep implements OnInit {

    constructor(public registrationService : RegistrationService) {
      super(registrationService);
    }


  public ngOnInit() { 
    this.stepName = 'sorry-wizard-panel';
    this.title = 'Sorry';
    this.instructionalText = 'Lorem Ipsum......';
  }

  public onBeforeNavigateNext = (): string => {
      return null;      
  }
}
