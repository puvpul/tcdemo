import { Component, OnInit } from '@angular/core';
import { WizardStep } from '../wizard-step';
import { RegistrationService } from '../services/registration.service';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'mydna-customerreg-start-step',
  templateUrl: 'start-step.component.html',
  styleUrls: ['start-step.component.css']
})
export class StartStepComponent extends WizardStep implements OnInit {

    public hasKit: string;
    public showError: boolean;

    constructor(public registrationService : RegistrationService) {
      super(registrationService);
    }


  public ngOnInit() { 
    this.stepName = 'start-step';
    this.title = 'Do you have a kit?';
    this.instructionalText = '';

    if (this.registrationService.data.barcode)
      this.hasKit = 'true';
  }

  public onBeforeNavigateNext = (): string => {
      if (this.hasKit == 'true')
        return 'default';
      else if (this.hasKit == 'false')    
      {
        this.registrationService.data.barcode = null;  
        return 'sorry-wizard-panel';
      }
      else
      {
        this.registrationService.error = 'Please make a selection below';
        return '';
      }
  }

  public validate() : boolean {
     return this.hasKit != null;
  }
}
