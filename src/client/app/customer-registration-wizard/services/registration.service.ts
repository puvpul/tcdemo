import {Injectable} from '@angular/core';
import { Registration } from '../models/registration';

@Injectable()
export class RegistrationService {
    private currentStepName: string;
    private currentStepTitle: string;
    private currentStepInstructionalText : string;
    error: string;
    
    data : Registration;
    hasStartedProperly: boolean;

    constructor() {
        this.data = new Registration();
        console.log('Registration Service Initialized...');
        
    }

    getData() {
        return this.data;
    } 

    setData(d:any) {
        this.data = d;
    }

    setCustomerDetails(custDetails:any){
        this.data.CustomerDetails = custDetails;
    }

    getCustomerDetails(){
        return this.data.CustomerDetails;
    }

    setCurrentStepName(stepName : string) {
        if (this.currentStepName != stepName) {
            this.currentStepName = stepName;
            this.error = '';
        }        
    }

    getCurrentStepName = () => this.currentStepName;

    setCurrentStepTitle = (title: string)  => this.currentStepTitle = title;
  
    getCurrentStepTitle = () => this.currentStepTitle;

    setCurrentStepInstructionalText = (instructionalText : string) => this.currentStepInstructionalText = instructionalText;

    getCurrentStepInstructionalText = () => this.currentStepInstructionalText;

}