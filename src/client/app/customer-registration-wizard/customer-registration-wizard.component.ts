import { Component, OnInit, AfterViewInit, ViewChild, Input } from '@angular/core';
import { WizardStep } from './wizard-step';

import { StartStepComponent } from './start-step/start-step.component';
import { MykitStepComponent } from './mykit-step/mykit-step.component';
import { CustomerDetailsStepComponent } from './customer-details-step/customer-details-step.component'
import { SelectReportsStepComponent} from './select-reports-step/select-reports-step.component';
import { PaymentStepComponent} from './payment-step/payment-step.component';
import { MedicationsStepComponent } from './medications-step/medications-step.component';
import { MyPractitionersStepComponent } from './my-practitioners-step/my-practitioners-step.component';
import { LoginDetailsStepComponent} from './login-details-step/login-details-step.component';
import { ConfirmationStepComponent} from './confirmation-step/confirmation-step.component';
import { RegistrationService } from './services/registration.service';

import { SorryWizardPanel } from './sorry-wizard-panel/sorry-wizard-panel.component';

import { Router, Route, ActivatedRoute, RouterOutlet } from '@angular/router';
/**
 * This class represents the lazy loaded AboutComponent.
 */

@Component({
  moduleId: module.id,
  selector: 'mydna-customerreg',
  templateUrl: 'customer-registration-wizard.component.html',
  styleUrls: ['customer-registration-wizard.component.css'],
  providers: [RegistrationService]
})
export class CustomerRegistrationWizardComponent implements OnInit, AfterViewInit {

  workflow: string = ''; 


  @ViewChild(StartStepComponent) startStep: StartStepComponent;
  @ViewChild(MykitStepComponent) mykitStep: MykitStepComponent;
  @ViewChild(CustomerDetailsStepComponent) customerDetailsStep: CustomerDetailsStepComponent;
  @ViewChild(SelectReportsStepComponent) selectReportsStep: MykitStepComponent;
  @ViewChild(MedicationsStepComponent) medicationsStep: MedicationsStepComponent;
  @ViewChild(MyPractitionersStepComponent) myPractitionersStep: MyPractitionersStepComponent;
  @ViewChild(LoginDetailsStepComponent) LoginDetailsStepComponent: LoginDetailsStepComponent;
  @ViewChild(PaymentStepComponent) paymentStepComponent: PaymentStepComponent;
  @ViewChild(ConfirmationStepComponent) confirmationStep: ConfirmationStepComponent;
  @ViewChild(SorryWizardPanel) sorryWizardPanel: SorryWizardPanel;
  
  public currentStep: string;

  constructor(private registrationService: RegistrationService, private route: ActivatedRoute) {
  }

  public ngOnInit() {
    var initialPage = (this.getInitialPage());   
    this.registrationService.setCurrentStepName(initialPage);  
  }

  private getInitialPage() : string {
    var url : string = this.route.snapshot.url.join('/'); 
    if (url.indexOf('customer-registration') >= 0)
      return "start-step";
    else if (url.indexOf('kit-registration') >= 0)
      return "mykit-step";
    // else if (url.indexOf('cusotmer-details') >= 0)
    //   return "cusotmer-details-step";
    else 
      return "start-step";       
  }

  public ngAfterViewInit() {
  }

  public onNext() {
    var childViewNext = this.getCurrentStep().onBeforeNavigateNext(); 
    if ( childViewNext == '' || !childViewNext)
      return;
    else if ( childViewNext == 'default') {
      var nextStep = this.getNextStep();
      if (nextStep) {
        this.registrationService.setCurrentStepName(nextStep);
      }
    }
    else 
      this.registrationService.setCurrentStepName(childViewNext);
  }

  public onPrevious() {
    var childViewPrevious = this.getCurrentStep().onBeforeNavigatePrevious(); 
    if ( childViewPrevious == '' || !childViewPrevious)
      return;
    else if ( childViewPrevious == 'default') {
      var previousStep = this.getPreviousStep();
      if (previousStep) {
        this.registrationService.setCurrentStepName(previousStep);
      }
    }
    else 
      this.registrationService.setCurrentStepName(childViewPrevious);
  }

  public getPreviousStep() {
    if (this.registrationService.getCurrentStepName() == this.getInitialPage())
      return '';
    else switch (this.registrationService.getCurrentStepName()) {
      case 'mykit-step': return 'start-step';  
      case 'customer-details-step': return 'mykit-step';
      case 'select-reports-step': return 'customer-details-step';
      case 'payment-step': return 'select-reports-step';
      case 'medications-step': return 'payment-step';
      case 'my-practitioners-step': return 'medications-step';
      case 'login-details-step': return 'my-practitioners-step';
      case 'confirmation-step': return 'login-details-step';
      default: return '';
    }
  }

  public getNextStep() {    
    switch (this.registrationService.getCurrentStepName()) {
      case 'start-step': return 'mykit-step';
      case 'mykit-step': return 'customer-details-step';
      case 'customer-details-step': return 'select-reports-step';
      case 'select-reports-step': return 'payment-step';
      case 'payment-step': return 'medications-step';
      case 'medications-step': return 'my-practitioners-step';
      case 'my-practitioners-step': return 'login-details-step';
      case 'login-details-step': return 'confirmation-step';
      default: return '';
    }
  }

  public getWizardStep(name: string) : WizardStep {
    switch (name) {
      case 'start-step': return this.startStep;
      case 'mykit-step': return this.mykitStep;
      case 'customer-details-step': return this.customerDetailsStep;
      case 'select-reports-step': return this.selectReportsStep;
      case 'payment-step': return this.paymentStepComponent;
      case 'medications-step': return this.medicationsStep;
      case 'my-practitioners-step': return this.myPractitionersStep;
      case 'login-details-step': return this.LoginDetailsStepComponent;
      case 'confirmation-step': return this.confirmationStep;
      case 'sorry-wizard-panel': return this.sorryWizardPanel;
      default: return null;
    }
  }

  public validate() : boolean {
    var result = false;
    var currentStep = this.getCurrentStep();
    if (currentStep) {
      result = currentStep.validate(); 
    }
    return result;
  }

  public getCurrentStep(): WizardStep {
    var currentStepName : string = this.registrationService.getCurrentStepName();
    var currentStep =  this.getWizardStep(currentStepName);
    if (currentStep != undefined)
      return currentStep;
    else 
      return null; 
  }
}