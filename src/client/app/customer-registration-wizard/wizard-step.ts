import { Component, AfterViewInit } from '@angular/core';
import { RegistrationService } from './services/registration.service'

export class WizardStep
{
    public stepName: string;
    public title: string;
    public instructionalText: string;

    public constructor(public registrationService: RegistrationService) {

    }

    public onBeforeNavigateNext = (): string => {
        return 'default';
    }

    public onBeforeNavigatePrevious = (): string => {
        return 'default';
    }

    public ngAfterViewInit() {
        this.registrationService.setCurrentStepTitle(this.title);
        this.registrationService.setCurrentStepInstructionalText(this.instructionalText);
    }

    public validate() : boolean {
        return true;
    }
}