import { Component, OnInit } from '@angular/core';
import { WizardStep } from '../wizard-step';
import { RegistrationService } from '../services/registration.service';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'mydna-customerreg-my-practitioners-step',
  templateUrl: 'my-practitioners-step.component.html',
  styleUrls: ['my-practitioners-step.component.css']
})
export class MyPractitionersStepComponent extends WizardStep implements OnInit {

    constructor(public registrationService : RegistrationService) {
      super(registrationService);
    }

  public ngOnInit() { 
    this.stepName = 'my-practitioners-step';
    this.title = 'My Kit';
    this.instructionalText = 'Lorem Ipsum......';
  }
}
