import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms'
import { WizardStep } from '../wizard-step';
import { RegistrationService } from '../services/registration.service';

//Third party
import { TextMaskModule } from 'angular2-text-mask';
//Components
import { DateDropDownComponent } from './../shared/date-dropdown/date-dropdown.component';
//Shared
import { MyDnaUtilityService } from './../shared/mydna-utility/mydna-utility.service'
import { MyDnaValidationPatterns } from './../shared/mydna-validation/mydna-validation-patterns'

/**
 * This class represents the lazy loaded CustomerDetailsStepComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'mydna-customerreg-customer-details-step',
  templateUrl: 'customer-details-step.component.html',
  styleUrls: ['customer-details-step.component.css'],
  providers: [MyDnaUtilityService,MyDnaValidationPatterns]
})
export class CustomerDetailsStepComponent extends WizardStep implements OnInit {

  @ViewChild('f') public myForm: NgForm;

  frm: any;

  showErrors: boolean;
  public CountryCode: string;
  iscontactRequired: boolean;
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]

  emailPattern: any;
  phonePattern: any;
  postcodePattern: string;
  mobilePattern: string;
  namePattern: string;

  constructor(public registrationService: RegistrationService,
     private myDnaValidationPatterns:MyDnaValidationPatterns,
     private myDnaUtilityService:MyDnaUtilityService
    ) {
    super(registrationService);
  }

  public ngOnInit() {
    this.stepName = 'customer-details-step';
    this.title = 'Personal Information';
    this.instructionalText = '';

    this.frm = {};

    this.CountryCode = "";
    this.iscontactRequired = false;
    this.showErrors = false;

    //validtors
    this.emailPattern = this.myDnaValidationPatterns.email;
    this.phonePattern = this.myDnaValidationPatterns.phone;
    this.postcodePattern = this.myDnaValidationPatterns.postcode;
    this.mobilePattern = this.myDnaValidationPatterns.mobile;
    this.namePattern = this.myDnaValidationPatterns.name;

    var cutomerDetails = this.registrationService.getCustomerDetails();

    if (cutomerDetails != undefined && cutomerDetails != null) {
      this.frm = cutomerDetails;
      //this.atLeastOneContactisEntered();
      this.setCountryCode();
      this.reFormatPhoneNumberDetails();
    }

  }

  public onBeforeNavigateNext = (): string => {
   
    if (this.myForm.valid) {
      this.formatPhoneNumber();
      this.registrationService.setCustomerDetails(this.frm);
      return 'default';
    } else {
      this.showErrors = true;
      return null;
    }
  }

  public onBeforeNavigatePrevious = (): string => {
    this.showErrors = false;
    this.formatPhoneNumber();
    this.registrationService.setCustomerDetails(this.frm);
    return 'default';
  }

  public validate(): boolean {
    return true;
  }

  private formatPhoneNumber(): void {
    if (this.frm.Phone !== undefined) {
      this.frm.Phone = this.CountryCode + this.frm.Phone;
    }
    if (this.frm.Mobile !== undefined) {
      this.frm.Mobile = this.CountryCode + this.frm.Mobile;
    }
  }

  private reFormatPhoneNumberDetails(): void {
    var phoneObject = this.myDnaUtilityService.splitPhoneNumber(this.frm.Phone, this.frm.Country);
    if (phoneObject && phoneObject.PhoneNumber != "") {
      this.frm.Phone = phoneObject.PhoneNumber;
    }
    var mobileObject =  this.myDnaUtilityService.splitPhoneNumber(this.frm.Mobile, this.frm.Country);
    if (mobileObject && mobileObject.PhoneNumber != "") {
      this.frm.Mobile = mobileObject.PhoneNumber;
    }
  }

  private setCountryCode() {
    if (this.frm.Country && this.frm.Country === 'Australia') {
      this.CountryCode = "+61"
    } else if (this.frm.Country && this.frm.Country === 'Canada') {
      this.CountryCode = "+1"
    } else {
      this.CountryCode = ""
    }
  }

  private atLeastOneContactisEntered() {
   
    if ((this.frm.Phone === null || this.frm.Phone === "") && (this.frm.Mobile ===null || this.frm.Mobile === "")) {

      this.iscontactRequired = true;
    } else {
      this.iscontactRequired = false;
    }
  }


}
