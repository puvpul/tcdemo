import { Component, OnInit } from '@angular/core';
import { WizardStep } from '../wizard-step';
import { RegistrationService } from '../services/registration.service';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'mydna-customerreg-select-reports-step',
  templateUrl: 'select-reports-step.component.html',
  styleUrls: ['select-reports-step.component.css']
})
export class SelectReportsStepComponent extends WizardStep implements OnInit {

    constructor(public registrationService : RegistrationService) {
      super(registrationService);
    }

  public ngOnInit() { 
    this.stepName = 'select-reports-step';
    this.title = 'Select Reports';
    this.instructionalText = 'Lorem Ipsum......';
  }
}
