import { Component, OnInit , EventEmitter} from '@angular/core';
import { WizardStep } from '../wizard-step';
import { RegistrationService } from '../services/registration.service';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'mydna-customerreg-mykit-step',
  templateUrl: 'mykit-step.component.html',
  styleUrls: ['mykit-step.component.css']
})
export class MykitStepComponent extends WizardStep implements OnInit {

   private barcode : string; 

    constructor(public registrationService : RegistrationService) {
      super(registrationService);
    }

  public ngOnInit() { 
    this.stepName = 'mykit-step';
    this.title = 'Please enter the barcode that came with your Kit';
    this.instructionalText = '';

    this.barcode = this.registrationService.data.barcode;
  }

  public onBeforeNavigateNext = (): string => {
        if (this.validate()) {
          this.registrationService.data.barcode = this.barcode;
          return 'default';          
        } else 
          return '';        
  }

  public validate() : boolean {
      return (this.barcode != null && this.barcode.length > 0);
  }

  
}
