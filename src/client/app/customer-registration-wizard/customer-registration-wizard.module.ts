import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerRegistrationWizardRoutingModule } from './customer-registration-wizard-routing.module';
import { FormsModule } from '@angular/forms';

//import { TextMaskModule } from 'angular2-text-mask';
import { DateDropDownModule} from './shared/date-dropdown/date-dropdown.module'
import { MyDnaUtilityModule } from './shared/mydna-utility/mydna-utility.module'
import { MyDnaValidationModule } from './shared/mydna-validation/mydna-validation.module'


import { RegistrationService }  from './services/registration.service';

import { CustomerRegistrationWizardComponent } from './customer-registration-wizard.component';
import { StartStepComponent } from './start-step/start-step.component';
import { MykitStepComponent } from './mykit-step/mykit-step.component';
import { CustomerDetailsStepComponent } from './customer-details-step/customer-details-step.component'
import { SelectReportsStepComponent} from './select-reports-step/select-reports-step.component';
import { PaymentStepComponent} from './payment-step/payment-step.component';
import { MedicationsStepComponent } from './medications-step/medications-step.component';
import { MyPractitionersStepComponent } from './my-practitioners-step/my-practitioners-step.component';
import { LoginDetailsStepComponent} from './login-details-step/login-details-step.component';
import { ConfirmationStepComponent} from './confirmation-step/confirmation-step.component';
import { SorryWizardPanel } from './sorry-wizard-panel/sorry-wizard-panel.component';




@NgModule({
  imports: [CommonModule, CustomerRegistrationWizardRoutingModule, FormsModule,
 //TextMaskModule,
  DateDropDownModule,
  MyDnaUtilityModule,
  MyDnaValidationModule
  ],
  declarations: [
    CustomerRegistrationWizardComponent, 
    StartStepComponent, 
    MykitStepComponent,
    CustomerDetailsStepComponent, 
    SelectReportsStepComponent,
    PaymentStepComponent,
    MedicationsStepComponent,
    MyPractitionersStepComponent,
    LoginDetailsStepComponent,
    ConfirmationStepComponent,
    SorryWizardPanel,  
    ],
  exports: [CustomerRegistrationWizardComponent],
  providers: [RegistrationService]  
})
export class CustomerRegistrationWizardModule { }
